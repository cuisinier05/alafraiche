# Installation

## 1st step
Cloner le dépot en local

## 2nd step
### Dans un terminal ouvert dans le dossir du projet
* composer install
* npm install
* npm run dev

## 3rd step
Changer les identifiants de votre base de données pour la connexion mysql dans le fichier ".env"

## 4th step
### Dans un terminal 
* php bin/console doctrine:database:create
* php bin/console doctrine:migrations:migrate

## 5th step
### Dans un terminal
### Lancer le serveur l'application
* symfony server:start

L'application devrait se lance dans votre navigateur.  
Faites un test en vous inscrivant et en vous connectant.  
Ensuite, signalez votre ressenti par le bouton page d'accueil.  
Consulter votre profil ou bien la carte globale des ressentis.  

ENJOY
