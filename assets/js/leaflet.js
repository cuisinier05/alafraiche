var map, newMarker, markerLocation;
// Délimitation de la carte et des zooms sur Grenoble
var sudOuest = L.latLng(45.133571,5.672365);
var nordEst = L.latLng(45.234223,5.836036);
var bounds = L.latLngBounds(sudOuest, nordEst);
var map = L.map('map', {
  center: [45.1922, 5.7268],
  maxBounds: bounds,
  minZoom: 8 ,
  maxZoom: 20,
  zoom: 15
});
// Fond de carte streets
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

map.createPane('labels');
map.getPane('labels').style.pointerEvents = 'none';
map.getPane('labels').style.zIndex = 650;

var positronLabels = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}{r}.png', {
        attribution: '©OpenStreetMap, ©CartoDB',
        pane: 'labels'
}).addTo(map);

map.on('click', addMarker);
// // ESRI searchbar
// var searchControl = new L.esri.Controls.Geosearch().addTo(map);

// var results = new L.LayerGroup().addTo(map);
//     // Esri function
//     searchControl.on('results', function(data){
//       results.clearLayers();
//       for (var i = data.results.length - 1; i >= 0; i--) {
//         results.addLayer(L.marker(data.results[i].latlng));
//         //ajout d'un marker à la carte quand on recherche
//         // new L.marker(data.results[i].latlng).addTo(map).openPopup();
//       }
//     })
//     // Send lng,lat to reverse geocoder.
//     fetch(`https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?f=pjson&langCode=EN&location=${e.latlng.lng},${e.latlng.lat}`)
//     .then(res => res.json())
//     .then(myJson => {
//       newMarker.bindPopup(myJson.address.LongLabel).openPopup();
//       console.log(myJson.address);
//     });
function addMarker(e){
    // Capture des coordonnées en X et Y du pointeur de la souris
    var x = e.latlng.lat;
    var y = e.latlng.lng;
    // Ajout des coordonnéees du marqueur aux data de la div qui a l'id 'map'
    $('#form_lat').val(x);
    $('#form_lon').val(y);
    // Add a Leaflet marker at the point of click
    var newMarker = new L.marker(e.latlng).addTo(map);

    // Send lng,lat to reverse geocoder.
    fetch(`https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?f=pjson&langCode=EN&location=${e.latlng.lng},${e.latlng.lat}`)
    .then(res => res.json())
    .then(myJson => {
      newMarker.bindPopup(myJson.address.LongLabel).openPopup();
      console.log(myJson.address);
    });
}