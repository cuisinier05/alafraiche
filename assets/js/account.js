// var map = L.map('map').setView([45.1922, 5.7268], 6);

// Délimitation de la carte et des zooms sur Grenoble
var sudOuest = L.latLng(45.133571,5.672365);
var nordEst = L.latLng(45.234223,5.836036);
var bounds = L.latLngBounds(sudOuest, nordEst);
var map = L.map('map', {
  center: [45.1922, 5.7268],
  maxBounds: bounds,
  minZoom: 8 ,
  maxZoom: 20,
  zoom: 15
});

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

//Selection de chaque div avec la classe marker
let ressentis = $('.ressenti');
// Tableaux des valeurs des champs lat et lng
let markerLat = $('.markerLat');
let markerLng = $('.markerLng');

// Les ressentis des markers
let couleurMarkers = $('.feelings');

// Définition des marqueurs oranges
var orangeIcon = new L.Icon({
  iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-orange.png',
  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});

// Boucle sur le tableau des ressentis et changement couleur orange ou rouge selon la valeur du ressenti
couleurMarkers.each( function( i ) {
  
  let couleur = $( this ).val();
  if (couleur === "PC" || couleur === "C") {
    new L.marker( [markerLat[i].defaultValue,markerLng[i].defaultValue ],{icon: orangeIcon}).addTo(map);
  } else {
    new L.marker( [markerLat[i].defaultValue,markerLng[i].defaultValue ]).addTo(map);  
  }

})

// for (let i = 0; i < ressentis.length; i++) {
//   var newMarker = new L.marker( [markerLat[i].defaultValue,markerLng[i].defaultValue]).addTo(map);
// }

// Génerer des pastilles à côté des cadeaux si user a tant de points
var pastille = document.getElementsByClassName('pastille')


if (user_points >=5 && user_points <10 ) {
  pastille[0].style.visibility= "visible";
}
else if (user_points >= 10 && user_points <15) {
pastille[1].style.visibility= "visible";

}
else if (user_points >= 15 && user_points <20) {
pastille[2].style.visibility= "visible";
} 

else if (user_points >= 20 && user_points <25) {
pastille[3].style.visibility= "visible";
}