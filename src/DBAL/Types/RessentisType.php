<?php
namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;
use Doctrine\DBAL\Types;

final class RessentisType extends AbstractEnumType
{
    public const FROID = 'F';
    public const PEU_FROID = 'PF';
    public const PEU_CHAUD = 'PC';
    public const CHAUD = 'C';

    protected static $choices = [
        self::FROID => 'Frais',
        self::PEU_FROID => 'Un peu frais',
        self::PEU_CHAUD => 'Un peu chaud',
        self::CHAUD => 'Chaud',
    ];
}