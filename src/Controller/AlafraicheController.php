<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Map;
use App\Entity\Contact;
use App\Entity\Ressenti;
use App\Entity\Gifts;

use App\Form\UserType;

use App\DBAL\Types\RessentisType;

use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Form\FormTypeInterface;

use Symfony\Component\HttpFoundation\Request;
// use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\RedirecTtoRoute;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AlafraicheController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {   
        return $this->render('alafraiche/index.html.twig', [
            'controller_name' => 'alafraicheController',
        ]);
    }

    /**
     * @Route("/account", name="account")
     */
    public function account()
    {
        $this->em = $this->getDoctrine()->getManager();
        $current_user = $this->getUser();
        
        $user_points = $current_user->getPoints();
        $user_statut = $current_user->getStatut();
        $user_username = $current_user->getUsername();
        

        
        $marker_user_repository = $this->em
        ->getRepository(User::class);

        //id de l'utilisateur courant
        $current_user_id = $current_user->getId();

        // Liste de tout les ressentis de l'utilisateur
        $user_Ressenti = $current_user->getRessentis();

        // $listUserMarkers = $marker_user_repository->findAll();

        return $this->render('alafraiche/account.html.twig', [
            'controller_name' => 'alafraicheController',
            'user_points' => $user_points,
            'user_statut' => $user_statut,
            'user_username' => $user_username,
            'user_Ressenti' => $user_Ressenti,
        ]);
    }

    /**
     * @Route("/map", name="map")
     */
    public function marker(Request $request)
    {
        $this->em = $this->getDoctrine()->getManager();
        $marker_repository = $this->em
        ->getRepository(Ressenti::class);

        $listMarkers = $marker_repository->findAll();

        return $this->render('alafraiche/map.html.twig', [
            'controller_name' => 'alafraicheController',
            'listMarkers' => $listMarkers,
        ]);
    }

    /**
     * @Route("/mailto", name="mailto")
     */
    public function mailto(Request $request)
    {
        $formulaire = new Contact();
        $this->em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder($formulaire)
        ->add('message', TextareaType::class)
        ->getForm();
    
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $current_user = $this->getUser();
            $contact = $form->getData();
            
            $contact->setUsers($current_user);

            $this->em->persist($contact);
            $this->em->flush();
            
            return $this->redirectToRoute('mailto');
        }

        {
        return $this->render('alafraiche/mailto.html.twig', [
            'controller_name' => 'alafraicheController',
            'form' => $form->createView(),
        ]);
        }
    }

    /**
     * @Route("/form", name="form")
     */
    public function Ressenti(Request $request, UserInterface $user, EntityManagerInterface $em )
    {
        $formulaire = new Ressenti();
        $this->em = $em;
        /**
         * rajouter une image dans le label et la relier que si l'on clique dessus, ça check la case
         */
        $form = $this->createFormBuilder($formulaire)
        
        ->add('ressenti', ChoiceType::class, [
            'label'    => 'Votre ressenti',
            'choices' => RessentisType::getChoices(),
        ])
        ->add('remarque', TextType::class, [
            'label'    => 'Que rajouteriez vous pour vous sentir mieux ?' ,
            'required' => false,
        ])
        ->add('lat', NumberType::class, [
            'label' => ' ',
        ])
        ->add('lon', NumberType::class, [
            'label' => ' ',
        ])
        ->getForm();
    
        $form->handleRequest($request);

        // Vérification du formulaire
        if ($form->isSubmitted() && $form->isValid()) {
            $contact = $form->getData();

            // Récuperation de l'utilisateur courant et passage à $formulaire pour la relation
            $current_user = $this->getUser();
            $formulaire->setUserId($current_user);

            // Récuperer les points de l'utilisateur courant et rajouter 1 point à l'utilisateur courant qui a envoyé un ressenti
            $user_points = $current_user->getPoints();

            // Points par rapport au statut de l'utilisateur courant
            if ($user_points < 30 ) {
                $current_user->setPoints(++$user_points);
            } else if ($user_points >= 30 && $user_points < 60 ) {
                $current_user->setStatut('GENERAL');
                $current_user->setPoints($user_points += 2);
            } else if($user_points >= 60){
                $current_user->setStatut('AMBASSADEUR');
                $current_user->setPoints($user_points += 3);
            }

            $formulaire->setCreatedAt( new \DateTime);

            $this->em->persist($contact);
            $this->em->persist($formulaire);
            $this->em->flush();

            $this->addFlash('success', 'Ressenti envoyé avec succès !');
            return $this->redirectToRoute('form');
        }
        
        {
        return $this->render('alafraiche/form.html.twig', [
            'controller_name' => 'alafraicheController',
            'form' => $form->createView(),
        ]);
        }
    }
    /**
     * @Route("/howto", name="howto")
     */
    public function howto()
    {

        return $this->render('alafraiche/howto.html.twig', [
            'controller_name' => 'alafraicheController',
        ]);
    }
    /**
     * @Route("/gifts", name="gifts")
     */
    public function gifts()
    {   
        $giftsManager = $this->em = $this->getDoctrine()->getManager();
        $giftsRepo = $this->em->getRepository(gifts::class);

        
        $listgifts = $giftsRepo->findAll();

        $current_user = $this->getUser();

        // $listgifts = $current_user->getgifts();
        $currentPoints = $current_user->getPoints();

        switch($currentPoints){
            case 5:
            $current_user->addGiftsId($listgifts[0]);
            break;
            case 10:
            $current_user->addGiftsId($listgifts[1]);
            break;
            case 15:
            $current_user->addGiftsId($listgifts[2]);
            break;
            case 20:
            $current_user->addGiftsId($listgifts[3]);
            break;
        }
        
        $tabgifts = $current_user->getGiftsId();

        return $this->render('alafraiche/gifts.html.twig', [
            'controller_name' => 'alafraicheController',
            'tabgifts' => $tabgifts,
            'listgifts' => $listgifts,
            ]);
    }
}