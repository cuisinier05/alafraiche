<?php

namespace App\Controller;

use App\Entity\User;

use App\Form\RegistrationType;
use Symfony\Component\HttpFoundation\Request;
// use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\RedirecTtoRoute;

class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="security_registration")
     */
    public function registration(Request $request, UserPasswordEncoderInterface $encoder, EntityManagerInterface $em){
        $user = new User();
        $this->em = $em;

        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user,$user->getPassword());
            $hash_confirm = $encoder->encodePassword($user, $user->getConfirmPassword());

            $user->setPassword($hash);
            $user->setConfirmPassword($hash_confirm);

            $user->setStatut('SOLDAT');
            $user->setPoints(0);
            $user->setUsername($user->getFirstname() . ' ' . $user->getLastname());

            $this->em->persist($user);
            $this->em->flush();

            return $this->redirectToRoute('security_login');
        }

        return $this->render('security/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     *@Route("/connexion", name="security_login") 
     */
    public function login(){
        return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/deconnexion", name="security_logout")
     */
    public function logout(){
        
    }
}
