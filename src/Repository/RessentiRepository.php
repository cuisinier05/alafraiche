<?php

namespace App\Repository;

use App\Entity\Ressenti;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Ressenti|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ressenti|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ressenti[]    findAll()
 * @method Ressenti[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RessentiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ressenti::class);
    }

    // /**
    //  * @return Ressenti[] Returns an array of Ressenti objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ressenti
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
